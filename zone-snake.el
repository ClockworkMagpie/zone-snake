;;; zone-snake.el --- Snake zone program

;; Copyright (C) 2012  Clockwork Magpie Studios, LLC

;; Author: Alex Midgley <alex@clockworkmagpie.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; includes shuffle-vector
(require 'cookie1)

(defvar zone-snake-head-char ?☺)
(defvar zone-snake-cutter-head-char ?☻)
(defvar zone-snake-dead-char ?†)
(defvar zone-snake-bizarro-dead-char ?☥)
(defvar zone-snake-count 5
  "*The number of snakes")
(defvar zone-snake-cutter-ratio 20
  "*Percent of snakes that can cut other snakes")
(defvar zone-snake-bizarro-ratio 25
  "*Percent of snakes that eat non-alphanumeric characters")
(defvar zone-snake-behaviors [zone-snake-behavior-straight-random])

(defconst zone-snake-base-block-table
  (let ((table (make-char-table 'block-table)))
    (set-char-table-range table ?\  t)
    (set-char-table-range table ?\n t)
    (set-char-table-range table ?\r t)
    table)
  "The characters which are not blocking to all snake types.")

(defconst zone-snake-alphanum-block-table
  (let ((table (make-char-table 'block-table)))
    (set-char-table-parent table zone-snake-base-block-table)
    (set-char-table-range table '(?a . ?z) t)
    (set-char-table-range table '(?A . ?Z) t)
    (set-char-table-range table '(?0 . ?9) t)
    (set-char-table-range table ?\. t)
    (set-char-table-range table zone-snake-bizarro-dead-char t)
    table))

(defconst zone-snake-bizarro-block-table
  (let ((table (make-char-table 'block-table)))
    (set-char-table-parent table zone-snake-base-block-table)
    (set-char-table-range table t t)
    (set-char-table-range table '(?a . ?z) nil)
    (set-char-table-range table '(?A . ?Z) nil)
    (set-char-table-range table '(?0 . ?9) nil)
    (set-char-table-range table zone-snake-bizarro-dead-char nil)
    table))

(defun zone-snake-new (&optional pt)
  (zone-snake-reset (make-vector 6 nil) nil pt))

(defmacro zone-snake-head (snake)
  `(car (zone-snake-tail ,snake)))

(defmacro zone-snake-tail (snake)
  `(elt ,snake 0))

(defmacro zone-snake-dir (snake)
  `(elt ,snake 1))

(defmacro zone-snake-block-table (snake)
  `(elt ,snake 2))

(defmacro zone-snake-cutterp (snake)
  `(elt ,snake 3))

(defmacro zone-snake-turn-behavior (snake)
  `(elt ,snake 4))

(defmacro zone-snake-replaced-char (snake)
  `(elt ,snake 5))

(defun zone-snake-reset (snake &optional snakes newhead)
  ;; kill the last head
  (if (not (null (zone-snake-head snake)))
      (cwm-putch (zone-snake-head snake) (zone-snake-get-tombstone snake)))

  (setf (zone-snake-block-table snake)
        (if (> zone-snake-bizarro-ratio (random 100))
            zone-snake-bizarro-block-table
          zone-snake-alphanum-block-table))

  (setf (zone-snake-cutterp snake)
        (> zone-snake-cutter-ratio (random 100)))

  (setf (zone-snake-turn-behavior snake) (cwm-choice zone-snake-behaviors))

  (while (or (null newhead)
             (snake-util-pt-in-snakes newhead snakes))
    (setq newhead (cons (random (window-width))
                        (random (1- (window-height))))))

  ;; Move the character that is in the head position if it's a
  ;; non-blocking character
  (let ((ch (cwm-get-char newhead)))
    (when (not (zone-snake-blockp snake ch))
      (cwm-putch newhead ?\ )
      (when (or (search-forward " " nil t)
                (progn (goto-char (point-min))
                       (search-forward " " nil t)))
        (delete-char -1)
        (insert ch))))

  ;; Save the character we're replacing if it's not a space
  (setf (zone-snake-replaced-char snake)
        (let ((ch (cwm-get-char newhead)))
          (when (not (char-equal ch ?\ )) ch)))

  (setf (zone-snake-tail snake) (list newhead))
  (cwm-putch newhead (zone-snake-get-head-char snake))

  snake)

(defun zone-snake-get-head-char (snake)
  (if (zone-snake-cutterp snake)
      zone-snake-cutter-head-char
    zone-snake-head-char))

(defun zone-snake-get-tombstone (snake)
  (or (zone-snake-replaced-char snake)
      (if (zone-snake-bizarrop snake)
          zone-snake-bizarro-dead-char
        zone-snake-dead-char)))

(defun zone-snake-bizarrop (snake)
  (eq (zone-snake-block-table snake) zone-snake-bizarro-block-table))

(defun zone-snake-blockp (snake ch)
  (not (aref (zone-snake-block-table snake) ch)))

(defun zone-snake-get-turns (snake)
  (funcall (zone-snake-turn-behavior snake) snake))

(defun zone-snake-behavior-straight-random (snake)
  (let ((directions (append (shuffle-vector (vector 'left 'up 'right 'down)) nil))
        (straight (zone-snake-dir snake)))
    (if straight
        (cons straight directions)
      directions)))

(defun zone-snake-behavior-lefty (snake)
  (case (zone-snake-dir snake)
    (left '(down left up right))
    (up '(left up right down))
    (right '(up right down left))
    (down '(right down left up))))

(defun zone-snake-move (snake pos)
  "Move the head of snake to pos. This assumes pos is not blocked."

  (push pos (zone-snake-tail snake))
  (let ((last-cons (zone-snake-tail snake)))
    (while (cdr last-cons)
      (if (cadr last-cons)
          (progn
            (cwm-swap-chars (car last-cons) (cadr last-cons))

            (when (null (cddr last-cons))
              (let* ((old-tail (cadr last-cons))
                     (tail-ch (cwm-get-char old-tail)))
                ;; Replace tombstones with spaces
                (when (snake-util-tombstonep tail-ch)
                  (cwm-putch old-tail ?\ )
                  (setq tail-ch ?\ ))

                ;; Drop space characters
                (when (aref zone-snake-base-block-table tail-ch)
                  (setcdr last-cons nil))))

            (setq last-cons (cdr last-cons)))

        (setcdr last-cons nil)))))

(defun zone-snake-pt-blocked (snake new-head-pt snakes)
  (or (eql new-head-pt (zone-snake-head snake))
      (cwm-pt-off-window new-head-pt)
      (zone-snake-blockp snake (cwm-get-char new-head-pt))
      (snake-util-pt-in-snakes new-head-pt snakes snake t)))

(defun zone-snake-next-head (snake snakes)
  (let ((directions (zone-snake-get-turns snake))
        new-head-pt
        (last-head (zone-snake-head snake)))
    (while (and (not new-head-pt) directions)
      (setq new-head-pt (cwm-new-pt last-head (car directions)))
      (if (not (zone-snake-pt-blocked snake new-head-pt snakes))
          (setf (zone-snake-dir snake) (car directions))
        (setq new-head-pt nil))
      (pop directions))
    new-head-pt))
            
(defun cwm-fill-screen (width char)
  (goto-char (window-start))
  (let ((start-line (line-number-at-pos)))
    (while (not (= (window-height) (- (line-number-at-pos) start-line)))
      (let ((line-size (- (line-end-position) (line-beginning-position))))
        (end-of-line)
        (if (> line-size width)
            (progn
              (goto-char (line-end-position))
              (delete-char (- width line-size)))
          (insert-char char (- width line-size)))
        (if (= (point-max) (point))
            (newline)
          (forward-line))))))

(defun cwm-calc-pt (pt)
  (if (null pt)
      (error "Null point"))
  (let ((x (car pt))
        (y (cdr pt)))
    (+ (window-start) x (* y (1+ (window-width))))))

(defun cwm-goto-x-y (pt)
    (goto-char (cwm-calc-pt pt)))

(defun cwm-new-pt (pt dir)
  (let ((delta
         (ecase dir
           (left '(-1 . 0))
           (up '(0 . -1))
           (right '(1 . 0))
           (down '(0 . 1)))))
    (cons (+ (car delta) (car pt)) (+ (cdr delta) (cdr pt)))))

(defun cwm-swap-chars (p1 p2)
  (let ((ch2 (cwm-get-char p2))
        ch)
    (cwm-goto-x-y p1)
    (setq ch (cwm-get-char nil))
    (delete-char 1)
    (insert ch2)
    (cwm-putch p2 ch)))

(defun cwm-get-char (pt)
  (let ((p (if (null pt)
               (point)
             (cwm-calc-pt pt))))
    (elt (buffer-substring p (1+ p)) 0)))

(defun cwm-copy-pt (pt)
  (cons (car pt) (cdr pt)))

(defun cwm-choice (seq)
  (elt seq (random (length seq))))

(defun cwm-pt-off-window (pt)
  (let ((x (car pt))
        (y (cdr pt)))
    (or (< x 0) (>= x (window-width))
        (< y 0) (>= y (1- (window-height))))))

(defun snake-util-pt-in-snakes (pt snakes &optional snake allow-cut)
  (let (result)
    (while snakes
      (let ((other-snake (car snakes)))
        (let ((self (eq snake other-snake))
              (other-tail (member pt (zone-snake-tail other-snake))))
          (if other-tail
              (setq result (if (and allow-cut
                                    (not self)
                                    (zone-snake-cutterp snake)
                                    (not (eq (zone-snake-head other-snake) (car other-tail))))
                               ;; cut the other snake
                               (setf (car other-tail) nil
                                     (cdr other-tail) nil)
                             t)
                    snakes nil)
            (setq snakes (cdr snakes))))))
    result))

(defun snake-util-tombstonep (ch)
  (or (char-equal ch zone-snake-dead-char)
      (char-equal ch zone-snake-bizarro-dead-char)))

(defun cwm-putch (pt ch)
  (cwm-goto-x-y pt)
  (delete-char 1)
  (insert ch))

(defun zone-pgm-snake ()
  (let (snakes)
    (cwm-fill-screen (window-width) ?\ )
    (set-window-start (selected-window) (point-min))

    (dotimes (i zone-snake-count)
      (push (zone-snake-new) snakes))

    (while (not (input-pending-p))
      (dolist (snake snakes)
        (let ((new-head-pt (zone-snake-next-head snake snakes))
              (last-tail (cwm-copy-pt (car (zone-snake-tail snake)))))

          (if (null new-head-pt)
              ;; The snake was trapped, reset and start somewhere else
              (zone-snake-reset snake snakes)
            (zone-snake-move snake new-head-pt))))
      (sit-for 0.1))))


(provide 'zone-snake)
;;; zone-snake.el ends here
