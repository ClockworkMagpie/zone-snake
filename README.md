Snake Zone Program
==========

What?
----------

This is a program for Emacs' zone mode in which snakes eat your
buffer. Some snakes eat alphanumeric characters, some eat everything
else. Some of the snakes can cut the other snakes!

Why?
----------

I was bored one day and started thinking about Centipede clones. "It
shouldn't be too hard to do that in Emacs," I foolishly thought. Once
I started, I couldn't stop, and now it's rather fascinating to watch.

How?
----------

Put `zone-snake.el` in your friendly neighborhood Elisp directory,
then add this to your `.emacs` file:

    :::common-lisp
    (require 'zone)
    (require 'zone-snake)
    ;; If you only want the snake program to run, use this instead:
    ;; (setq zone-programs [zone-pgm-snake])
    (setq zone-programs (vconcat zone-programs [zone-pgm-snake]))
    ;; This will turn on zone after two minutes of idle time
    (zone-when-idle 120)

Setting `zone-snake-count` will change the number of snakes, and
`zone-snake-cutter-ratio` controls how many snakes are cutters.
